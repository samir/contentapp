from WebApp_spaw import WebApp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""



# Son Class of the Class "WebApp"

class ContentApp(WebApp):

    # Known Resources of the App
    content_spaw = {"/": "Main Page, ask whatever you want, when you finish write End",
               "/Who si the CEO of this project": "Samir Spaw ",
               "/Did you have breakfast? ": "Not yet",
               "/End": "Enjoy the app :)"}

    # Function that extracts the relevant information of the Request
    def Analyze(self, request):
        NameOfResource = request.split(' ', 2)[1]
        return NameOfResource


    def Compute(self, analyzedRequest):
        # 1ºCase -> Known Resource
        if analyzedRequest in self.content:
            cont = self.content[analyzedRequest]
            ans = PAGE.format(cont=cont)
            code = "200 OK"
        # 2ºCase -> Unknown Resource
        else:
            ans = PAGE_NOT_FOUND.format(resource=analyzedRequest)
            code = "404 NOT FOUND"
        return code, ans

# Creation of the "Content App"
if __name__ == "__main__":
    webApp = ContentApp('localhost', 2023)