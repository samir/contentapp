import socket

class webApp:
    """Root of a hierarchy of classes implementing web applications
    This class does almost nothing. Usually, new classes will
    inherit from it, and by redefining "parse" and "process" methods
    will implement the logic of a web application in particular.
    """
    # This function is the responsible os analyze the information of the Request
    def Analyze (self, request):
        """Parse the received request, extracting the relevant information."""

        print("Parse: Not parsing anything")
        return None
    # This function has the purpose of processing the information of the previous function
    def compute (self, parsedRequest):
        """Process the relevant elements of the request.
        Returns the HTTP code for the reply, and an HTML page.
        """

        print("Process: Returning 200 OK")
        return ("200 OK", "<html><body><h1>It works!</h1></body></html>")

    # WebApp class constructor
    def __init__ (self, host, port):
        """Initialize the web application."""
        self.host = host
        self.port = port
        # Create a TCP objet socket and bind it to a port
        self.socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.host, self.port))

        # Queue a maximum of 5 TCP connection requests
        mySocket.listen(5)

        # Accept connections, read incoming data, and call
        # parse and process methods (in a loop)

        while True:
            print("Waiting for connections...")
            (recvSocket, address) = self.socket.accept()
            print("HTTP request received (going to parse and process):")
            request =
            analyzedRequest = self.Analyze(recvSocket.recv(2048).decode('utf8'))
            (returnCode, htmlAnswer) = self.compute(analyzedRequest)
            print("Answering back...")
            response = "HTTP/1.1 " + returnCode + " \r\n\r\n" \
                       + htmlAnswer + "\r\n"
            recvSocket.send(response.encode('utf8'))
            print("---------------------------------------------------")
            recvSocket.close()
